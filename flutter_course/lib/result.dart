import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int scoreRes;
  final Function resetHandler;

  Result(this.scoreRes, this.resetHandler);

  String get resStr {
    String text = 'finished! ';
    if (this.scoreRes < 2) {
      text += "You lost";
    } else {
      text += "Winner!";
    }
    return (text);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(resStr,
              style: TextStyle(fontSize: 38, fontWeight: FontWeight.bold)),
          FlatButton(
            child: Text('Restart Quiz'),
            textColor: Colors.blue,
            onPressed: resetHandler,
          )
        ],
      ),
    );
  }
}
