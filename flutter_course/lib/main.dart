import 'package:flutter/material.dart';
import './questions.dart';
import './answer.dart';
import './quiz.dart';
import './result.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questIndex = 0;
  var _totalScore = 0;
  var _quests = [
    {
      'questionText': 'what\'s your fav color?',
      'answers': [
        {'text': 'b', 'score': 10},
        {'text': 'r', 'score': 40}
      ]
    },
    {
      'questionText': 'what\'s your fav animal?',
      'answers': [
        {'text': 'bb', 'score': 10},
        {'text': 'tt', 'score': 40}
      ]
    }
  ];
  void ansIndex(int score) {
    if (_questIndex <= _quests.length) {
      print('more plz');
    }
    _totalScore += score;
    setState(() {
      _questIndex++;
    });
    print(_questIndex);
  }

  void restartQuiz() {
    setState(() {
      _totalScore = 0;
      _questIndex = 0;
    });
  }

  void _ansQuest() {
    print('chosen');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('first app'),
        ),
        body: _questIndex < _quests.length
            ? Quiz(_quests, ansIndex, _questIndex)
            : Result(_totalScore, restartQuiz),
      ),
    );
  }
}
